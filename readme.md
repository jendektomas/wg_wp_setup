# base repo for wordpress installations

## local development
```
$ docker-compose up -d
```
Wordpress is accessible at [http://127.0.0.1:8080](http://127.0.0.1:8080).

Logs are accessible using following command:
```
$ docker-compose logs -f [wordpress|mariadb]
```

## db dump
```
$ docker-compose exec mariadb mysqldump -uroot -ppassword wordpress > wp_dump.sql
```

## Websupport Shell
### Init
```
$ ssh wsshell
$ git clone git@bitbucket.org:jendektomas/wg_wp_setup.git
$ cd wg_wp_setup
$ pip install -r requirements.txt
```

### Run
```
$ python3 install.py --domain onrails.cz --subdomain wordpress
```
