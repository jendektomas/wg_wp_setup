<?php
/**
 * Theme functions and definitions
 *
 * @package HelloElementorChild
 */

/**
 * Load child theme css and optional scripts
 *
 * @return void
 */
function hello_elementor_child_enqueue_scripts() {
	wp_enqueue_style(
		'hello-elementor-child-style',
		get_stylesheet_directory_uri() . '/style.css',
		[
			'hello-elementor-theme-style',
		],
		'1.0.0'
	);
}
add_action( 'wp_enqueue_scripts', 'hello_elementor_child_enqueue_scripts', 20 );

/* Add custom code to head */
add_action('wp_head', 'add_to_head');
function add_to_head(){
?>
<!-- HEADER CODE -->
<?php
};


/* Add custom code to body start  */
add_action('wp_body_open', 'add_to_body');
function add_to_body(){
?>
<!-- BODY START CODE -->
<?php
};

/* Add custom code to footer */ 
add_action('wp_footer', 'add_to_footer');
 function add_to_footer(){ 
?> 
<!-- FOOTER CODE -->

function ele_disable_page_title( $return ) {
   return false;
}
add_filter( 'hello_elementor_page_title', 'ele_disable_page_title' );

<?php
 };