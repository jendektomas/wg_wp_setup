��    0      �  C         (     )     ;     A     I     P     W     `  	   i  :   s  	   �     �     �     �     �     �     �     �  A     X   H  7   �  v   �      P  +   q     �     �     �     �     �     �     �  	   �  	   �     �  	   �     �     �       %     5   C     y     �     �  
   �  S   �       �   
     �  l  �     
     1
     8
     A
     M
  	   S
  
   ]
  	   h
  D   r
     �
     �
     �
     �
     �
     �
     �
       Q     C   k  7   �  �   �  '   m  3   �  
   �  	   �  	   �     �     �     �     �     �          !     *     ;     [     n  2   �  .   �  
   �  ,   �          "  k   .     �  �   �     m     	      0   .   '   (             )   ,       "   #                                         !                    +                  %             
   *   /                   $                           -   &                               -- select page -- 1 day 1 month 1 week 1 year 3 months 6 months Animation Are you sure you want to reset these settings to defaults? Bar color Bottom Button style Button text Colors Cookie Notice Custom link Deactivation Enable if you want all plugin data to be deleted on deactivation. Enable to give to the user the possibility to refuse third party non functional cookies. Enable to reload the page after the notice is accepted. Enter non functional cookies Javascript code here (for e.g. Google Analitycs) to be used after the notice is accepted. Enter the cookie notice message. Enter the full URL starting with http(s):// Fade Footer Header Message No None Ok On scroll Page link Position Reloading Reset to defaults Script blocking Script placement Select from one of your site's pages. Select where all the plugin scripts should be placed. Settings Settings restored to defaults. Slide Text color To get the user consent status use the <code>cn_cookies_accepted()</code> function. Top We use cookies to ensure that we give you the best experience on our website. If you continue to use this site we will assume that you are happy with it. infinity PO-Revision-Date: 2020-02-21 11:35:39+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n == 1) ? 0 : ((n >= 2 && n <= 4) ? 1 : 2);
X-Generator: GlotPress/3.0.0-alpha.2
Language: sk
Project-Id-Version: Plugins - Cookie Notice &amp; Compliance for GDPR / CCPA - Stable (latest release)
 -- zvoľte stránku -- 1 deň 1 mesiac 1 týždeň 1 rok 3 mesiace 6 mesiacov Animácia Ste si istí, že chcete obnoviť nastavenia na predvolené hodnoty? Farba lišty Dole Štýl tlačidla Text tlačidla Farby Cookie Notice Vlastný odkaz Deaktivácia Povoľte, ak chcete aby všetky dáta modulu boli odstránené pri deaktivácií. Dajte používateľom možnosť odmietnuť cookies tretích strán. Povoliť stránku znovu načítať po prijatí cookies. Vložte sem nefunkcionálny cookies Javascript kód (napríklad Google Analytics). Tento kód bude použitý po akceptovaní cookies. Vložte správu o používaní cookies. Zadajte celú URL adresu začínajúcu s http(s):// Vystúpiť Pätička Hlavička Správa Nie Žiadna Ok Pri skrolovaní Odkaz na stránku Pozícia Načítať znovu Obnoviť predvolené nastavenia Blokovanie scriptu Umiestnenie skriptu Zvoľte jednu zo stránok vašej webovej stránky. Zvoľte, kde má byť kód modulu umiestnený. Nastavenia Nastavenie obnovené na predvolené hodnoty. Slide Farba textu Pre získanie statusu prijatia cookies používateľom použite funkciu <code>cn_cookies_accepted()</code>. Hore Používame cookies aby sme pre vás zabezpečili ten najlepší zážitok z našich webových stránok. Ak budete pokračovať v používaní tejto stránky budeme predpokladať, že ste s ňou spokojní. navždy 