#!/usr/bin/env python

import argparse
import hashlib
import hmac
import json
import logging
import os
import subprocess
import sys
import time
import uuid
from datetime import datetime, timezone
from typing import ItemsView

import requests
from jinja2 import Template
from dotenv import load_dotenv

logging.basicConfig(format='[%(levelname)s] %(message)s', level=logging.INFO)
load_dotenv()

WEBSUPPORT_ACCESS_KEY = os.environ.get('WEBSUPPORT_ACCESS_KEY')
WEBSUPPORT_SECRET_KEY = os.environ.get('WEBSUPPORT_SECRET_KEY')
WEBSUPPORT_DATABASE_SERVERS = {
    'mariadb103': 'mariadb103.r1.websupport.sk:3313',
    'mariadb105': 'mariadb105.r1.websupport.sk:3315',
}

def exec(cmd):
    p = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    stdout = p.stdout.read().decode("utf-8").split('\n')
    stderr = p.stderr.read().decode("utf-8").split('\n')
    return (stdout, stderr)

def stdout_to_tty(msg):
    return '\n'.join(msg)

def uuid4_to_string(replace_dash=False):
    uuid_out = uuid.uuid4()
    if replace_dash == True:
        uuid_out = str(uuid_out).replace('-', '')
    return uuid_out

def ws_api_request(path, method='get', query=None, payload=None):    
    timestamp = int(time.time())
    api = "https://rest.websupport.sk"
    query = "" # query part is optional and may be empty
    apiKey = WEBSUPPORT_ACCESS_KEY
    secret = WEBSUPPORT_SECRET_KEY
    canonicalRequest = "%s %s %s" % (method.upper(), path, timestamp)
    signature = hmac.new(bytes(secret, 'UTF-8'), bytes(canonicalRequest, 'UTF-8'), hashlib.sha1).hexdigest()
    
    headers = {
        "Content-Type": "application/json",
        "Accept": "application/json",
        "Date": datetime.fromtimestamp(timestamp, timezone.utc).isoformat()
    }
    
    if method == 'get':
        r = requests.get("%s%s%s" % (api, path, query), headers=headers, auth=(apiKey, signature))
    
    if method == 'post':
        r = requests.post("%s%s%s" % (api, path, query), headers=headers, json=payload, auth=(apiKey, signature))
    
    return r

# run when file called directly
if __name__ == '__main__':
    
    # initilize parser
    parser = argparse.ArgumentParser(usage="%(prog)s [OPTIONS]", description="Perform automated installation of Wordpress from Git repo")
    parser.add_argument("--domain", action="store", required=True)
    parser.add_argument("--subdomain", action="store", required=True)
    args = parser.parse_args()

    # construct path to hosting folder including the subdomain
    hosting_path = os.path.join('..', args.domain, 'sub', args.subdomain)
    logging.info(f'Hosting path is calculated to "{hosting_path}"')

    # check if hosting folder exists
    hosting_path_domain = os.path.join('..', args.domain, 'sub')
    if not os.path.exists(hosting_path_domain):
        logging.error(f'Hosting folder "{hosting_path_domain}" doesn\'t exists. Stopping...')
        sys.exit(1)

    # check if folder already exists. if so, just stop.
    if os.path.exists(hosting_path):
        logging.error(f'Folder "{hosting_path}" already exists. Stopping...')
        sys.exit(1)

    # check if required domain is to be found in websupport
    hosting_ids = [(x['id'], x['name']) for x in ws_api_request('/v1/user/self/hosting').json()['items']]

    # seach for the domain in all hostings, haf!
    vhost_id = int(-1)
    
    for item in hosting_ids:
        hosting_id, hosting_name = item
        r = ws_api_request(f'/v1/user/self/hosting/{hosting_id}/vhost').json()['items']
        for item in r:
            logging.info(f'Searching hosting "{hosting_name}"')
            if item['domain'] == args.domain:
                vhost_id = item['id']
                hosting_id_db = hosting_id
                logging.info(f'Found vhost ("{args.domain}") in hosting "{hosting_name}" ({hosting_id})')
                break
    
    # if above hyper-turbo optimizied loop didn't find any hosting, raise the hell (i mean error)
    if vhost_id == -1:
        logging.error(f'Unable to find "{args.domain}" in any Websupport hosting')
        sys.exit(1)

    # try to create new database in websupport
    db_name = uuid4_to_string(replace_dash=True)[0:12]
    database_properties = {
        "domain": args.domain,
        "dbName": db_name,
        "dbPassword": uuid4_to_string(replace_dash=True),
        "dbType": "mariadb105",
        "dbUser": db_name,
        "defaultCollation": "utf8_general_ci",
        "note": "Generated for {}.{} using Webgate's install.py".format(args.subdomain, args.domain)
    }
    logging.info(f'Creating DB with name/user "{db_name}".')
    r = ws_api_request(f'/v1/user/self/hosting/{hosting_id_db}/db', method='post', payload=database_properties).json()
    
    # check if creation of databse was successful
    if r['status'] != 'success':
        logging.error(str(r['errors']))
        sys.exit(1)

    # set database credentials from websupport rest api response
    database_credentials = r["item"]

    # copy docroot to hosting folder
    logging.info(f'Started to copy docroot to "{hosting_path}"')
    cp, cp_stderr = exec('cp -r ./html ' + hosting_path) 

    # if there is any stderr, as it shouldn't be, raise error
    if len(cp_stderr) > 1:
        logging.error(f'Copying to folder "{hosting_path}" failed.')
        
        for item in cp_stderr:
            logging.error(item)
        logging.error('Stopping...')
        sys.exit(1)

    # generate random salts
    logging.info('Generating salts')
    wp_config_salts = [uuid.uuid4() for x in range(0, 8)]

    # render sample file and store it to variable, which later will be saved to file
    wp_config_sample_path = os.path.join(hosting_path, 'wp-config-sample.php')
    with open(wp_config_sample_path) as file_:
        template = Template(file_.read())
    db_host = WEBSUPPORT_DATABASE_SERVERS[database_properties['dbType']].split(':')[0]
    db_port = WEBSUPPORT_DATABASE_SERVERS[database_properties['dbType']].split(':')[1]
    db_pass = database_properties['dbPassword']
    wp_config_rendered = template.render(database_credentials=database_credentials, db_host=db_host, db_pass=db_pass, db_port=db_port, wp_config_salts=wp_config_salts, args=args)
    
    
    # write rendered file to final path
    with open(os.path.join(hosting_path, 'wp-config.php'), 'w') as file_:
        file_.write(wp_config_rendered)
    logging.info('Templated "wp-config.php" with real values.')
    
    # wait 10 seconds to give db time to accept connections
    logging.info('Sleeping for 10 seconds...')
    time.sleep(10)

    # import database
    logging.info('Importing SQL dump to database')
    mysql_import, mysql_import_stderr = exec(f'mysql -h{db_host} -P{db_port} -u{db_name} -p{db_pass} {db_name} < wp_dump.sql') 

    # if there is any stderr, as it shouldn't be, raise error
    if len(mysql_import_stderr) > 3:
        print(mysql_import_stderr)
        sys.exit(1)
    
    logging.info(f'All seems to be done. Try to check your new Wordpress installation at http://{args.subdomain}.{args.domain}')
